<html>
    <head>
        <title>Votaci&oacute;n mejor jugador liga ACB</title>
        <link href="estilos.css" rel="stylesheet" type="text/css" />
        <%@ page import="java.util.Map" %>
    </head>
    <body class="resultado">
        <font size=10>
        Votaci&oacute;n al mejor jugador de la liga ACB
        <hr>
        </font>
        <br>
        <h1>tabla de votacion</h1>
        <%
        Map<String, Integer>  nombreP = (Map<String, Integer> ) session.getAttribute("resultados");
        %>
        <table name="tabla">
            <thead>
                <tr>
                    <td>Nombre</td>
                    <td>Cantidad</td>
                </tr>
            </thead>
            <tbody>
                
                <% for (Map.Entry<String, Integer> entry : nombreP.entrySet()) { %>
                    <tr>
                        <td><%= entry.getKey() %></td>
                        <td><%= entry.getValue() %></td>
                    </tr>
                <% } %>
              
            </tbody>
        </table>
        <br> <a href="index.html"> Ir al comienzo</a>
    </body>
</html>
