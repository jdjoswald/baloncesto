import java.sql.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gargoylesoftware.htmlunit.javascript.host.Console;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ModeloDatos {

    private Connection con;
    private Statement set;
    private ResultSet rs;
    String titulo="El error es:";
    private static final Logger logger = Logger.getLogger(ModeloDatos.class.getName());


    public void abrirConexion() {
       
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            // Con variables de entorno
            String dbHost = System.getenv().get("DATABASE_HOST");
            String dbPort = System.getenv().get("DATABASE_PORT");
            String dbName = System.getenv().get("DATABASE_NAME");
            String dbUser = System.getenv().get("DATABASE_USER");
            String dbPass = System.getenv().get("DATABASE_PASS");

            String url = dbHost + ":" + dbPort + "/" + dbName;
            con = DriverManager.getConnection(url, dbUser, dbPass);

        } catch (Exception e) {
            // No se ha conectado
            logger.log(Level.SEVERE, "Ocurrio un error: " + e.getMessage(), e);
          
        }
    }

    public boolean existeJugador(String nombre) {
        boolean existe = false;
        String cad;
        try {
            set = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores");
            while (rs.next()) {
                cad = rs.getString("Nombre");
                cad = cad.trim();
                if (cad.compareTo(nombre.trim()) == 0) {
                    existe = true;
                }
            }
            rs.close();
            set.close();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Ocurrio un error: " + e.getMessage(), e);
           
        }
        return (existe);
    }

    public void actualizarJugador(String nombre) {
        try {
            set = con.createStatement();
            set.executeUpdate("UPDATE Jugadores SET votos=votos+1 WHERE nombre " + " LIKE '%" + nombre + "%'");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No modifica la tabla
            logger.log(Level.SEVERE, "Ocurrio un error: " + e.getMessage(), e);
           
        }
    }

    public void insertarJugador(String nombre) {
        try {
            set = con.createStatement();
            set.executeUpdate("INSERT INTO Jugadores " + " (nombre,votos) VALUES ('" + nombre + "',1)");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No inserta en la tabla
            System.out.println("No inserta en la tabla");
           
        }
    }
    public void ponerACero() {
        try {
            set = con.createStatement();
            set.executeUpdate(" UPDATE Jugadores SET votos= 0;");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No inserta en la tabla
            System.out.println("No inserta en la tabla");
           
        }
    }

    public Map<String, Integer>  vertodos() {
        try {
            Map<String, Integer> myMap = new HashMap<>();
            set = con.createStatement();
            ResultSet rs = set.executeQuery("SELECT * FROM Jugadores;");
            while (rs.next()) {
                String nombre = rs.getString("nombre");
                int cantidad = rs.getInt("votos");
                myMap.put(nombre, cantidad);
                
            }
        
       
        rs.close();
        set.close();
        return myMap;
        } catch (Exception e) {
            Map<String, Integer> myMap = new HashMap<>();
            
            logger.log(Level.SEVERE, "Ocurrio un error: " + e.getMessage(), e);
            
            return myMap;
        }
    }

    public void cerrarConexion() {
        try {
            con.close();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Ocurrio un error: " + e.getMessage(), e);
        }
    }

}
