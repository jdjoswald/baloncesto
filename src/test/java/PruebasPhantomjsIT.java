import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import java.util.List;

public class PruebasPhantomjsIT{
    private static WebDriver driver=null;
    private ModeloDatos bd;
    
    @Test
    public void tituloIndexTest(){
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(), "El titulo no es correcto");
        System.out.println(driver.getTitle());
        driver.close();
        driver.quit();
    }

    @Test
    public void Vervotos(){
  
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        driver.findElement(By.id("volverCero")).click();
        driver.findElement(By.id("verTodo")).click();

        List<WebElement> columnElements = driver.findElements(By.xpath("//table//tbody//td[2]"));
        System.out.println("el numero de columnas es :"+columnElements.size());
        boolean tieneValorDiferenteDeCero = false;
        for (WebElement element : columnElements) {
        String valor = element.getText();
            if (!valor.equals("0")) {
                tieneValorDiferenteDeCero = true;
                break;
            }
        }
        assertEquals(false, tieneValorDiferenteDeCero, "boton no funciona");
        System.out.println("pf-a");

        driver.close();
        driver.quit();

      
        
    }

   @Test
    public void pfB() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        WebElement radioButton = driver.findElement(By.cssSelector("input[type='radio'][value='Otros']"));
        radioButton.click();
        WebElement input = driver.findElement(By.name("txtOtros"));
        input.sendKeys("Jugador nuevo");
        WebElement btn = driver.findElement(By.id("Votar"));
        btn.click();
        driver.findElement(By.id("atras")).click();
        driver.findElement(By.id("verTodo")).click();
        List<WebElement> rows = driver.findElements(By.tagName("tr"));
        System.out.println("el numero de filas es :"+rows.size());
        boolean existe = false;
        for (WebElement row : rows) {
            List<WebElement> cells = row.findElements(By.tagName("td"));
            String firstColumn = cells.get(0).getText();
            String secondColumn = cells.get(1).getText();
            if (firstColumn.equals("Jugador nuevo") && Integer.parseInt(secondColumn) == 1) {
                existe=true;
                break;
            }
        }
        assertEquals(true, existe, "no existe el jugador");
        System.out.println("pf-b");
        driver.close();
        driver.quit();       
    }
}
